const {checkAge, checkFullName} = require('../src/util.js');
const {assert} = require('chai');

describe('test_checkAge', ()=>{
    it('age_is_integer_value', ()=>{
        const age = 'A';
        assert.isNumber(checkAge(age));
    })

    it('age_not_empty', ()=>{
        const age = "";
        assert.isNotEmpty(checkAge(age));
    })

    it('age_not_undefined', ()=>{
        const age = undefined;
        assert.isDefined(checkAge(age));
    })

    it('age_is_not_null', ()=>{
        const age = null;
        assert.isNotNull(checkAge(age));
    })

    it('age_is_not_equal_to_0', ()=>{
        const age = 0;
        assert.notEqual(0,checkAge(age));
    })
})

describe('test_checkFullName', ()=>{
    it('fullName_not_empty', ()=>{
        const fullName = "";
        assert.isNotEmpty(checkFullName(fullName));
    })

    it('fullName_is_not_null', ()=>{
        const fullName = null;
        assert.isNotNull(checkFullName(fullName));
    })

    it('fullName_not_undefined', ()=>{
        const fullName = undefined;
        assert.isDefined(checkFullName(fullName));
    })

    it('fullName_is_string_value', ()=>{
        const fullName = 5;
        assert.isString(checkFullName(fullName));
    })

    it('fullName_length_is_not_equal_to_0', ()=>{
        const fullName = 5;
        assert.isString(checkFullName(fullName.length));
    })

})