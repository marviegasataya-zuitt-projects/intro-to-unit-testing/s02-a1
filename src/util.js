function checkAge(age){

    if(typeof(age) != 'number'){
        return "Error: Age is a number!";
    }

    if(age==""){
        return "Error: Age is Empty!";
    }

    if(age===undefined){
        return "Error: Age should be defined!";
    }

    if(age===null){
        return "Error: Age should not be null!";
    }

    if(age==0){
        return "Error: Age is Equal to Zero!";
    }

    return age;
}

function checkFullName(fullname){

    if(fullname==""){
        return "Error: full name is Empty!";
    }

    if(fullname===null){
        return "Error: full name should not be null!";
    }

    if(fullname===undefined){
        return "Error: full name should be defined!";
    }

    if(typeof(fullname) !== 'string'){
        return "Error: full name should be a string value!";
    }

    if(fullname.length==0){
        return "Error: full name length is Equal to Zero!";
    }

    return fullname;
}

module.exports = {
    checkAge: checkAge,
    checkFullName: checkFullName
}